#ifndef _ACTIVE_SET_H_
#define _ACTIVE_SET_H_

#include "optimizer.h"

namespace zyclincoln{
  namespace InSoMniA{

    template<>
    bool optimizer<func<soc> >::solve_active_set(Eigen::VectorXd& end_point, double& end_value,
                          const double threshold, const size_t max_iter, const std::set<size_t>& working_set){
      _iter_info._max_iter = max_iter;
      _iter_info._threshold = threshold;

      std::set<size_t> working_set_index = working_set;

      while(_iter_info._current_iter < _iter_info._max_iter 
        && _iter_info._last_value - _iter_info._current_value > threshold){
        // build kkt system from working set and rhs

        {
          std::cerr << "iter: " << _iter_info._current_iter << std::endl;
          std::cerr << "working_set: ";
          for(auto iter = working_set_index.cbegin(); iter != working_set_index.cend(); iter++){
            std::cerr << *iter << ", ";
          }
          std::cerr << std::endl;
          std::cerr << "current_point: " << _iter_info._current_point.transpose() << std::endl;
          std::cerr << "current_value: " << _iter_info._current_value << std::endl;
        }

        Eigen::MatrixXd hessian;
        _object->hessian(_iter_info._current_point, hessian);

        Eigen::MatrixXd A;
        A.resize(_eqc.size() + working_set_index.size(), hessian.cols());

        if(_eqc.size() > 0){
          Eigen::MatrixXd A1;
          build_A_from_eqc(_eqc, A1);
          A.block(0, 0, _eqc.size(), hessian.cols()) = A1;  
        }
        
        if(working_set_index.size() > 0){
          Eigen::MatrixXd A2;
          build_A_from_workingset(_ieqc, working_set_index, A2);
          A.block(_eqc.size(), 0, working_set_index.size(), hessian.cols()) = A2;
        }

        Eigen::VectorXd g;
        _object->gradient(_iter_info._current_point, g);
        
        Eigen::VectorXd h;
        h.resize(A.rows());

        if(_eqc.size() > 0){
          Eigen::VectorXd h1;
          build_h_from_eqc(_eqc, _iter_info._current_point, h1);
          h.segment(0, _eqc.size()) = h1;
        }
        if(working_set_index.size() > 0){
          Eigen::VectorXd h2;
          build_h_from_workingset(_ieqc, working_set_index, _iter_info._current_point, h2);
          h.segment(_eqc.size(), working_set_index.size()) = h2;
        }

        Eigen::VectorXd x; 
        trival_kkt_solver(hessian, A, g, h, x);

        Eigen::VectorXd p, lambda;
        p.resize(g.rows());
        lambda.resize(x.rows() - g.rows());
        p = -x.segment(0, g.rows());
        lambda = x.segment(g.rows(), x.rows() - g.rows());
        // std::cout << "p: " << p.transpose() << std::endl;
        // std::cout << "A: " << A << std::endl;
        // std::cout << "hessian: " << hessian << std::endl;
        // check the length of k
        if(p.norm() < 1e-10){
        // if k is zero

          // check if kkt parameter is satisfied
          // if yes, return
          // if no, remove one from working set
          auto min_iter = working_set_index.end();
          double min_lambda = 0;
          int i = 0;
          for(auto iter = working_set_index.begin(); iter != working_set_index.end(); iter++, i++){
            if(lambda(_eqc.size() + i, 0) < min_lambda){
              min_lambda = lambda(_eqc.size() + i, 0);
              min_iter = iter;
            }
          }

          if(min_iter != working_set_index.end()){
            working_set_index.erase(min_iter);
          }
          else{
            break;
          }
        }
        else{
        // else
          // calculate alpha from p

          // if meet constraints
          // add constraints into working set
          double alpha = 1;
          auto constraint_iter = _ieqc.end();
          int i = 0;
          for(auto iter = _ieqc.begin(); iter != _ieqc.end(); iter++, i++){
            if(working_set_index.find(i) != working_set_index.end())
              continue;
            Eigen::VectorXd gradient;
            (*iter)->gradient(_iter_info._current_point, gradient);
            // std::cout << "gradient: " << gradient.transpose() << std::endl;
            // std::cout << "gradient value decrease: " << gradient.transpose() * p << std::endl;
            if(gradient.transpose() * p >= 0)
              continue;

            double value;
            (*iter)->value(_iter_info._current_point, value);

            // if(value == 0)
            //   continue;

            double c_alpha = -value / (gradient.transpose() * p);
            // std::cerr << "p: " << p.transpose() << std::endl;
            // // std::cerr << "id: " << *iter << std::endl;
            // std::cerr << "gradient: " << gradient.transpose() << std::endl;
            // std::cerr << "value: " << value << std::endl;
            // std::cerr << "c_alpha: " << c_alpha << std::endl;

            assert(c_alpha > 0);
            if(c_alpha < alpha){
              alpha = c_alpha;
              constraint_iter = iter;
            }
          }

          _iter_info._last_value = _iter_info._current_value;
          _iter_info._current_point += alpha*p;

          _object->value(_iter_info._current_point, _iter_info._current_value);

          if(alpha != 1){
            working_set_index.insert(constraint_iter - _ieqc.begin());
          }
        }

        {
          std::cerr << "working_set: ";
          for(auto iter = working_set_index.cbegin(); iter != working_set_index.cend(); iter++){
            std::cerr << *iter << ", ";
          }
          std::cerr << std::endl;
          std::cerr << "current_point: " << _iter_info._current_point.transpose() << std::endl;
          std::cerr << "current_value: " << _iter_info._current_value << std::endl;

        }
        _iter_info._current_iter++;
      }

      end_point = _iter_info._current_point;
      end_value = _iter_info._current_value;
      return true;
    }
    

  }
}

#endif