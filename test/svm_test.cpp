#include <iostream>
#include <memory>
#include "Eigen/Core"
#include "optimizer/active_set.h"
#include "utility/function_utility.h"

using namespace zyclincoln::InSoMniA;
using namespace std;
using namespace Eigen;

int main(){

  MatrixXd sample_x;
  VectorXd sample_y;

  sample_x.resize(10, 1);
  sample_y.resize(10, 1);

  sample_x << 0, 2, 3, 4, 5, 6, 7, 8, 9, 10;
  sample_y << 1, 2, 3, 4, 5, 6, 7, 8, 9, 11;

  // sample_x.resize(3, 1);
  // sample_y.resize(3, 1);
  // sample_x << 1, 2, 3;
  // sample_y << 1, 2, 3;

  double C = 10;
  double epsilon = 0.1;

  size_t dim = sample_x.cols();
  size_t sample = sample_x.rows();

  MatrixXd obj_hessian;
  obj_hessian.resize(sample*2 + dim, sample*2 + dim);
  obj_hessian.setIdentity();

  VectorXd obj_gradient;
  obj_gradient.resize(sample*2 + dim); obj_gradient.setZero();
  for(int i = 1; i < obj_gradient.rows(); ++i){
    obj_gradient(i) = C;
  }

  double obj_constant = 0;

  shared_ptr<func<soc>> objectFunction = build_soc_function(obj_hessian, obj_gradient, obj_constant, 2*sample + dim);

  vector<shared_ptr<func<foc>>> ieqc;
  vector<shared_ptr<func<foc>>> eqc;

  Eigen::MatrixXd parameters;
  parameters.resize(sample*4, sample*2 + dim); parameters.setZero();
  for(int i = 0; i < sample_x.rows(); ++i){
    parameters(i, 0) = -sample_x(i, 0);
    parameters(i + sample, 0) = sample_x(i, 0);
    parameters(i, 1 + i) = 1;
    parameters(i + sample, 1 + sample + i) = 1;
    parameters(i + 2*sample, 1 + i) = 1;
    parameters(i + 3*sample, 1 + i + sample) = 1;
  }

  Eigen::VectorXd constants;
  constants.resize(sample*4); constants.setZero();
  for(int i = 0; i < sample_x.rows(); ++i){
    constants(i) = sample_y(i) + epsilon;
    constants(i + sample) = -sample_y(i) + epsilon;
  }

  build_foc_function_from_matrix(parameters, constants, 2*sample + dim, ieqc);

  optimizer<func<soc>> optimizer(objectFunction, eqc, ieqc);

  VectorXd initPoint;
  std::set<size_t> working_set;

  initPoint.resize(2*sample + dim); initPoint.setZero();
  for(int i = 0; i < sample_x.rows(); ++i){
    if(-sample_y(i) - epsilon > 0){
      initPoint(dim + i) = -epsilon - sample_y(i);
      working_set.insert(i); working_set.insert(i + 3*sample);
    }
    if(sample_y(i) - epsilon > 0){
      initPoint(dim + i + sample) = sample_y(i) - epsilon;
      working_set.insert(sample+i); working_set.insert(i + 2*sample);
    }
    else{
      working_set.insert(sample*2 + i); working_set.insert(sample*3 + i);
    }
  }

  optimizer.initialize(initPoint);

  VectorXd endPoint;
  double endValue;

  endPoint.resize(2*sample + dim);
  optimizer.solve_active_set(endPoint, endValue, 1e-10, 1000, working_set);

  cerr << "end point: " << endPoint.transpose() << endl;
  cerr << "end value: " << endValue << endl;

  return 0;

}